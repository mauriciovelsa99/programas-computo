/*-----------------------------------------------------------------------

Nombre: problema1_factorial.c
Fecha: 8 de Noviembre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que dael factorial de un número entero no negativo
usando for y while con incremento y decremento
con int el factorial máximo es 12
-----------------------------------------------------------------------*/
#include<stdio.h>

int main(){
  int number, version;
  do {
    printf("Escriba un numero entero no negativo para calcular su factorial\n");
    scanf("%d", &number);
  } while(number < 0 );
  do {
    printf("Escriba la versión que desea usar para calcular el factorial\n");
    printf("1 - while incremento\n");
    printf("2 - while decremento\n");
    printf("3 - for incremento\n");
    printf("4 - for decremento\n");
    scanf("%d", &version);
  } while(version < 1 || version > 4 );
  int answer = 1;
  int i;
  switch (version) {
    case 1: //while incremento
    i = 2;
      while(i<=number){
        answer*=i;
        i++;
      }
      break;
    case 2: //while decremento
      i = number;
      while(i>1){
        answer*=i;
        i--;
      }
      break;
    case 3: //for incremento
      for(i = 2;i<=number; i++) answer*=i;
      break;
    case 4: //for decremento
      for(i = number;i>1; i--) answer*=i;
      break;

  }
  printf("el factorial de %d es %d\n", number,answer);
  return 0;
}
