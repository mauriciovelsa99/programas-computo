/*-----------------------------------------------------------------------

Nombre: problema3_padovan.c
Fecha: 8 de Noviembre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que calcula el n-esimo termino de la sucesion de Padovan
-----------------------------------------------------------------------*/
#include<stdio.h>

int main(){
  int n, a=1,b=1,c=1,d;
  do {
    printf("Ingresa el termino que quieres calcular de la sucesion Padovan (comienza desde el termino 1)\n");
    scanf("%d",&n);
  } while(n <=0);
  for(int i = 4; i<=n; i++) {
    d = a+b;
    a = b;
    b = c;
    c = d;
  }
  printf("El termino numero %d de Padovan es %d\n", n, c);
  return 0;
}
