/*-----------------------------------------------------------------------

Nombre: problema4_perrin.c
Fecha: 8 de Noviembre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que calcula el n-esimo termino de la sucesion de Perrin
-----------------------------------------------------------------------*/
#include<stdio.h>

int main(){
  int n, a=3,b=0,c=2,d;
  do {
    printf("Ingresa el termino que quieres calcular de la sucesion Perrin (comienza desde el termino 1)\n");
    scanf("%d",&n);
  } while(n <=0);
  for(int i = 4; i<=n; i++) {
    d = a+b;
    a = b;
    b = c;
    c = d;
  }
  printf("El termino numero %d de Perrin es %d\n", n, (n==1)? a : (n==2)? b : c);
  return 0;
}
