/*-----------------------------------------------------------------------

Nombre: problema2_fibonacci.c
Fecha: 8 de Noviembre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que calcula el n-esimo termino de la sucesion de Fibonacci
-----------------------------------------------------------------------*/
#include<stdio.h>

int main(){
  int n, a=0,b=1;
  do {
    printf("Ingresa el termino que quieres calcular de la sucesion Fibonacci (comienza desde el termino 1)\n");
    scanf("%d",&n);
  } while(n <=0);
  for(int i = 3; i<=n; i++) {
    b = a+b;
    a = b-a;
  }
  printf("El termino numero %d de fibonacci es %d\n", n, (n==1)? a : b);
  return 0;
}
