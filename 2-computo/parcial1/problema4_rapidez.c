/*-----------------------------------------------------------------------

Nombre: problema4_rapidez
Fecha: 20 de septiembre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que ayuda a medir la rapidez de la computadora al contar

-----------------------------------------------------------------------*/
#include <stdio.h>

int main()
{
  for(long int i = 1; i<= 300000000; i++){
    if(i%100000000 == 0) {
      printf("%ld\n",i);
    }
  }
    return 0;
}
