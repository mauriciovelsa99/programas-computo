/*-----------------------------------------------------------------------

Nombre: problema1_mathmenu.c
Fecha: 24 de Agosto de 2020
Autor: JLFA y Mauricio Vélez
Descripcion: Programa que dado un menu de
opciones matematicas el programa ejecuta una
y solo una de las secciones escritas en el.

Opciones:

1.- Pedir un número y diga si es par o impar
2.- Pedir 3 números enteros y los muestre en pantalla de menor a mayor.
3.- Dado un número en radianes calcular el Coseno de ese número.
4.- Dado un número en radianes calcular el Seno de ese número.

-----------------------------------------------------------------------*/

// Bibliotecas

#include <stdio.h>
#include <cmath>
// Funciones
void esPar(int numero){
    if (numero%2 == 0){
        printf("El numero %d es par\n", numero);
    } else {
        printf("El numero %d es impar\n", numero);
    }
}

void imprimirAscendente(float a,float b,float c) {
  if (a>b){
    a = a+b;
    b = a-b;
    a = a-b;
    }
  if (a>c){
    a =	a+c;
    c = a-c;
    a =	a-c;
    }
  if (b>c){
    c =	c+b;
    b = c-b;
    c =	c-b;
    }
  printf("%f, %f, %f\n", a,b,c);

}
// Constantes

int main(){

	// Declaracion de variables

	int opcion, numero;
	float a,b,c, angulo,coseno, seno;

	// Informacion y Pedimento de los datos

	printf("\n");
	printf("\t ** PROGRAMA MATH MENU **\n");
 	printf("\t ** FUNCIONES MATEMATICAS A UTILIZAR **\n");
	printf("Selecione una opcion: \n");
	printf("1.- Pedir un número y diga si es par o impar \n");
	printf("2.- Pedir 3 números enteros y los muestre en pantalla de menor a mayor.\n");
	printf("3.- Dado un número en radianes calcular el Coseno de ese número.\n");
	printf("4.- Dado un número en radianes calcular el Seno de ese número.\n");
	printf("\n");

	opcion = 0;

	printf("Introduzca una opcion: ");
	scanf("%d",&opcion);


	// Procesamiento e impresion de los datos


	if(opcion == 1){

       printf("Introduzca un número: ");
	   scanf("%d", &numero);
	   esPar(numero);
	}else if(opcion == 2){

	  printf("Escribe 3 números y los imprimiré en orden ascendente: ");
	  scanf("%f %f %f", &a, &b, &c);
	  imprimirAscendente(a,b,c);

	}else if(opcion == 3){

	  printf("Introduce un ángulo (en radianes) para calcular el coseno: ");
	  scanf("%f", &angulo);
	  coseno = cos(angulo);
	  printf("El coseno del ángulo %f es %f\n", angulo, coseno);

	}else if(opcion == 4){

	  printf("Introduce un ángulo (en radianes) para calcular el seno: ");
	  scanf("%f", &angulo);
	  seno = sin(angulo);
	  printf("El coseno del ángulo %f es %f\n", angulo, seno);

	}else{

	  printf("\n No es una opcion del menu \n");

	}


	return 0;
}
