/*-----------------------------------------------------------------------

Nombre: problema2_datos_enteros.c
Fecha: 20 de septiembre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que muestra los limites de los tipos de datos enteros.

-----------------------------------------------------------------------*/
#include <stdio.h>
#include <limits.h>

int main()
{
  printf("El valor minimo de SHORT INT = %d\n", SHRT_MIN);
  printf("El valor maximo de SHORT INT = %d\n", SHRT_MAX);

  printf("El valor minimo de INT = %d\n", INT_MIN);
  printf("El valor maximo de INT = %d\n", INT_MAX);

  printf("El valor minimo de LONG = %ld\n", LONG_MIN);
  printf("El valor maximo de LONG = %ld\n", LONG_MAX);
    return 0;
}
