/*-----------------------------------------------------------------------

Nombre: problema3_sumagauss.c
Fecha: 24 de Agosto de 2020
Autor: Mauricio Vélez
Descripcion: Programa que hace una suma de gauss hasta el número solicitado

-----------------------------------------------------------------------*/
#include <stdio.h>

int main()
{
    unsigned long int sum = 0;
    int maxnum;
    printf("¿Hasta qué numero quieres sumar?\n");
    scanf("%d", &maxnum);
    for(int counter = 1; counter<=maxnum;counter++){
        sum += counter;
    }
    printf("suma : %ld\n", sum);
    return 0;
}
