/*-----------------------------------------------------------------------

Nombre: problema4.c
Fecha: 2 de Octubre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que calcula el valor e utilizando n términos de la serie e = 1 + 1/1! + 1/2! + 1/3! +...

-----------------------------------------------------------------------*/
#include <stdio.h>

long int factorial(int numero) {
  long int res = 1;
  for(int i = 2;i<=numero;i++){
    res *=i;
  }
  return res;
}

int main()
{
    long int n;
    double e = 1;
    do {
      printf("Ingrese el número de términos para obtener la aproximación de e\n" );
      scanf("%ld", &n);
    } while(n < 0 || n%1 != 0);
    for(long int i = 1; i<=n;i++){
      e+= 1.0/factorial(i);
    }
    printf("e = %lf\n", e);
    return 0;
}
