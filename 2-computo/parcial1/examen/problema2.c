/*-----------------------------------------------------------------------

Nombre: problema2.c
Fecha: 2 de Octubre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que imprime los multiplos de 3 desde 1 hasta n, con n dado por el usuario

-----------------------------------------------------------------------*/
#include <stdio.h>

int main()
{
    long int n;
    do {
      printf("Ingrese un numero entero no negativo para obtener los múltiplos de 3 desde 1 hasta n\n" );
      scanf("%ld", &n);
    } while(n < 0 || n%1 != 0);
    printf("Los múltiplos de 3 desde 1 hasta %ld son:\n", n);
    for(long int i = 3; i<=n;i+=3){
      printf("%ld\n", i);
    }
    return 0;
}
