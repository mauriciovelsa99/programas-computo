/*-----------------------------------------------------------------------

Nombre: problema1.c
Fecha: 2 de Octubre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que pide un número y multiplica o suma los 10 primeros naturales

-----------------------------------------------------------------------*/
#include <stdio.h>

int factorial(int numero) {
  int res = 1;
  for(int i = 2;i<=numero;i++){
    res *=i;
  }
  return res;
}
long int sumatoria_hasta_diez(int numero) {
  long int res = 0;
  for(int i = 1;i<=10;i++){
    res +=i;
  }
  return res;
}
int main()
{
    long int numero, fact, n;
    do {
      printf("Ingrese un numero entero, si supera a 10, se multiplican os 10 primeros naturales, si no, se suman\n" );
      scanf("%ld", &numero);
    } while(numero%1 != 0);
    if (numero > 10) {
      fact = factorial(10);
      printf("La multiplicación de los primeros 10 naturales es %ld\n", fact);
    }
    else {
      n = sumatoria_hasta_diez(numero);
      printf("La suma de los primeros 10 naturales es %ld\n", n);
    }
    return 0;
}
