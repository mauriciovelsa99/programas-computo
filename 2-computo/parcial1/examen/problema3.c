/*-----------------------------------------------------------------------

Nombre: problema3.c
Fecha: 2 de Octubre de 2020
Autor: Mauricio Vélez
Descripcion: Programa que obtiene el factorial de un número

-----------------------------------------------------------------------*/
#include <stdio.h>

int factorial(int numero) {
  int res = 1;
  for(int i = 2;i<=numero;i++){
    res *=i;
  }
  return res;
}

int main()
{
    long int numero, fact;
    do {
      printf("Ingrese un numero entero no negativo para obtener su factorial\n" );
      scanf("%ld", &numero);
    } while(numero < 0 || numero%1 != 0);
    fact = factorial(numero);
    printf("El factorial de %ld es %ld\n", numero, fact );
    return 0;
}
