/*
Nombre: angulo_mayor.c
Fecha: 11 de noviembre de 2020
Autor: Mauricio Vélez
Descripción: Calcula el angulo mayor de un triangulo con el teorema del coseno
*/

void AnguloMayor(double x1, double x2,double x3, double y1, double y2, double y3) {
  double anguloMayor, a = Perimetro(x1,x2,x3,y1,y2,y3, 'a'), b = Perimetro(x1,x2,x3,y1,y2,y3, 'b'), c = Perimetro(x1,x2,x3,y1,y2,y3, 'c'), A,B,C;
  A = acos( (b*b+c*c-a*a)/(2*b*c) );
  B = acos( (a*a+c*c-b*b)/(2*a*c) );
  C = M_PI - A - B;
  if (A>B && A> C) {
    anguloMayor = A;
  }else if( B> C) {
    anguloMayor = B;
  } else {
    anguloMayor = C;
  }
  printf("El angulo mayor es de: %lf radianes\n", anguloMayor);
}
