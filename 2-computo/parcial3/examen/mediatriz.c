/*
Nombre: mediatriz.c
Fecha: 11 de noviembre de 2020
Autor: Mauricio Vélez
Descripción: Calcula la ecuación de la mediatriz
*/

void Mediatriz(double x1, double x2,double x3, double y1, double y2, double y3) {
  double px = (x1+x2)/2, py = (y1+y2)/2, m;
  if (y2-y1 != 0 && x2-x1 != 0){
    m = (y2-y1)/(x2-x1);
    m = -1/m;
  } else {
    px = (x3+x2)/2, py = (y3+y2)/2;
    m = (y3-y2)/(x3-x2);
    m = -1/m;
  }
  printf("la ecuación de la mediatriz es: y-%lf = %lf(x-%lf)\n", py, m,px);
}
