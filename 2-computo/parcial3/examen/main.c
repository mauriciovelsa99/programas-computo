/*
Nombre: main.c
Fecha: 11 de noviembre de 2020
Autor: Mauricio Vélez
Descripción: Dados tres puntos en R^2, describe las características del triángulo (si es que hay):
1. Perímetro
2. Mediana (Ecuación)
3. Altura
4. Ángulo mayor
5. Mediatriz
6. Área
*/
#include <stdio.h>
#include <math.h>
#include "perimetro.c"
#include "altura.c"
#include "angulo_mayor.c"
#include "area.c"
#include "colineales.c"
#include "mediana.c"
#include "mediatriz.c"
double Perimetro(double x1, double x2,double x3, double y1, double y2, double y3, char lado);
int Colineales(double x1, double y1,double x2, double y2, double x3, double y3);
void Area(double x1, double x2,double x3, double y1, double y2, double y3);
void Altura(double x1, double x2,double x3, double y1, double y2, double y3);
void AnguloMayor(double x1, double x2,double x3, double y1, double y2, double y3);
void Mediana(double x1, double x2,double x3, double y1, double y2, double y3);
void Mediatriz(double x1, double x2,double x3, double y1, double y2, double y3);

int main(){
  //variables
  double x1,x2,x3,y1,y2,y3;
  char opcion;
  //información del usuario
  printf("--- PROGRAMA QUE CARACTERIZA UN TRIANGULO ---\n");
  printf("--- DADOS TRES PUNTOS EN EL PLANO ---\n");
  //datos de entrada
  printf("Ingrese el punto P1(x1,y1):\n");
  scanf("%lf,%lf", &x1,&y1);
  printf("Ingrese el punto P2(x2,y2):\n");
  scanf("%lf,%lf", &x2,&y2);
  printf("Ingrese el punto P3(x3,y3):\n");
  scanf("%lf,%lf", &x3,&y3);
  //Procesamiento e impresión
  if (Colineales(x1,y1,x2,y2,x3,y3)){
    printf("Son colineales\n");
    return 0;
  }
  //Escribir menu
  printf("\n ***********************************************\n");
  printf(" Este programa calcula las características siguientes:\n");
  printf("\t a) Perímetro \n");
  printf("\t b) Mediana \n");
  printf("\t c) Altura \n");
  printf("\t d) Angulo mayor \n");
  printf("\t e) Mediatriz \n");
  printf("\t f) Area \n");
  printf(" ***********************************************\n");
  printf("Introduzca una opcion: \n");
  scanf("%s", &opcion);
  switch (opcion){
    case 'a':
      Perimetro(x1,x2,x3,y1,y2,y3, ' ');
      break;
    case 'b':
      Mediana(x1,x2,x3,y1,y2,y3);
      break;
    case 'c':
      Altura(x1,x2,x3,y1,y2,y3);
      break;
    case 'd':
      AnguloMayor(x1,x2,x3,y1,y2,y3);
      break;
    case 'e':
      Mediatriz(x1,x2,x3,y1,y2,y3);
      break;
    case 'f':
      Area(x1,x2,x3,y1,y2,y3);
      break;
    case 'g':
      printf("La caracterízación del triángulo es: \n");
      Perimetro(x1,x2,x3,y1,y2,y3, ' ');
      Mediana(x1,x2,x3,y1,y2,y3);
      Altura(x1,x2,x3,y1,y2,y3);
      AnguloMayor(x1,x2,x3,y1,y2,y3);
      Mediatriz(x1,x2,x3,y1,y2,y3);
      Area(x1,x2,x3,y1,y2,y3);
    default:
      printf("No es una opcion valida\n");
  }
  return 0;
}
