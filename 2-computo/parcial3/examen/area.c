/*
Nombre: perimetro.c
Fecha: 11 de noviembre de 2020
Autor: Mauricio Vélez
Descripción: Calcula el area de un triangulo con la fórmula de Herón
*/

void Area(double x1, double x2,double x3, double y1, double y2, double y3) {
  double area, p = Perimetro(x1,x2,x3,y1,y2,y3, ' '), a = Perimetro(x1,x2,x3,y1,y2,y3, 'a'), b = Perimetro(x1,x2,x3,y1,y2,y3, 'b'), c = Perimetro(x1,x2,x3,y1,y2,y3, 'c');
  p/=2;
  area = sqrt(p*(p-a)*(p-b)*(p-c));
  printf("El area es: %lf\n", area);
}
