/*
Nombre: colineales.c
Fecha: 11 de noviembre de 2020
Autor: Mauricio Vélez
Descripción: Determinar si tres puntos son colineales

Si las pendientes son m1 = (y2-y1)/(x2-x1) y m2 = (y3-y2)/(x3-x2), entonces al ser colineales, m1=m2, es decir,
(y2-y1)/(x2-x1) = (y3-y2)/(x3-x2),
(y2-y1)*(x3-x2) = (y3-y2)*(x2-x1)
*/
double absolute(double number){
  return number < 0? -number: number;
}

int Colineales(double x1, double y1,double x2, double y2, double x3, double y3) {
  double epsilon = 0.0000001;
  double sonColineales = absolute((y2-y1)*(x3-x2) - (y3-y2)*(x2-x1)) < epsilon;
  return sonColineales;
}
