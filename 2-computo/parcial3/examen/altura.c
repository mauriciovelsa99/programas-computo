/*
Nombre: perimetro.c
Fecha: 11 de noviembre de 2020
Autor: Mauricio Vélez
Descripción: Calcula la ecuación de una altura del triángulo
*/

void Altura(double x1, double x2,double x3, double y1, double y2, double y3) {
  double pendienteLado = (y2-y1)/(x2-x1), pendienteAltura = -1/pendienteLado;
  printf("la ecuación de la altura es: y-%lf = %lf(x-%lf)\n", y3, pendienteAltura,x3);
}
