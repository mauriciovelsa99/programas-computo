/*
Nombre: perimetro.c
Fecha: 11 de noviembre de 2020
Autor: Mauricio Vélez
Descripción: Calcula el perimetro de un triangulo con lados a,b,c usando distancia euclideana
*/

double Perimetro(double x1, double x2,double x3, double y1, double y2, double y3, char lado) {
  double perimetro = 0;
  switch(lado){
    case 'a':
      return sqrt(pow( x2-x1,2) +pow(y2-y1,2));
      break;
    case 'b':
      return sqrt(pow( x3-x1,2) +pow(y3-y1,2));
      break;
    case 'c':
      return sqrt(pow( x3-x2,2) +pow(y3-y2,2));
      break;
    default: //return area
      perimetro = sqrt(pow( x2-x1,2) +pow(y2-y1,2)) + sqrt(pow( x3-x1,2) +pow(y3-y1,2)) + sqrt(pow( x3-x2,2) +pow(y3-y2,2));
      printf("El perimetro es: %lf\n", perimetro);
      return perimetro;
  }
}
