/*
Nombre: mediana.c
Fecha: 11 de noviembre de 2020
Autor: Mauricio Vélez
Descripción: Calcula la ecuación de la mediana
*/

void Mediana(double x1, double x2,double x3, double y1, double y2, double y3) {
  double px = (x1+x2)/2, py = (y1+y2)/2, m;
  m = (py-y3)/(px-x3);
  printf("la ecuación de la mediana es: y-%lf = %lf(x-%lf)\n", y3, m,x3);
}
