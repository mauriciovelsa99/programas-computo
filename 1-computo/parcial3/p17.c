/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para tomar dos números del 1 al 5 y decir si ambos son primos
 */
#include <stdio.h>
int main(){
  int a,b;
  do{
  printf("Ingresa dos enteros del 1 al 5 y te diré si ambos son primos\n");
  scanf("%d", &a);
  scanf("%d", &b);
  }while(a < 0 || b < 0 || a > 5 || b > 5);
  if (a == 1 || b == 1 || a == 4 || b == 4){
    printf("Al menos uno no es primo\n");
  }
  else{
    printf("Los dos son primos\n");
  }
  return 0;
}
