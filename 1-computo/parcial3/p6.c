/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para imprimir números en orden descendente
 */
#include <stdio.h>
int main(){
  float a,b,c;
  printf("Escribe 3 números separados por espacios y los imprimiré en orden descendente\n");
  scanf("%f %f %f", &a, &b, &c);
  if (a<b){
    a = a+b;
    b = a-b;
    a = a-b;
    }
  if (a<c){
    a =	a+c;
    c = a-c;
    a =	a-c;
    }
  if (b<c){
    c =	c+b;
    b = c-b;
    c =	c-b;
    }
  printf("%f, %f, %f\n", a,b,c);
  
  return 0;
}
