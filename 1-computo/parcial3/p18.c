/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para tomar dos números del 1 al 5 y decir si ambos son pares o impares
 */
#include <stdio.h>
int main(){
  int a,b;
  do{
  printf("Ingresa dos enteros del 1 al 5 y te diré si ambos son pares o impares\n");
  scanf("%d", &a);
  scanf("%d", &b);
  }while(a < 0 || b < 0 || a > 5 || b > 5);
  if (a % 2 == 0 && b%2 == 0){
    printf("Los dos son pares\n");
  }
  else if (a%2 ==1 && b%2 ==1){
    printf("Los dos son impares\n");
  }
  else{
    printf("Uno es par y otro impar\n");
  }
  return 0;
}
