/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para decir si un número del 0 al 5 es primo
 */
#include <stdio.h>
int main(){
  int numero;
  do {
    printf("Escribe un número del 1 al 5 y te diré si es primo\n");
    scanf("%d", &numero);
      }while(numero > 5 || numero <1);
  if(numero == 1 || numero == 4){
    printf("no es primo\n");
  } else{
    printf("es primo\n");
  }
  return 0;
}
