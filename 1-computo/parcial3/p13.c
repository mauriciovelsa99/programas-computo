/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para saber si tres números se han introducido en orden decreciente
 */
#include <stdio.h>
int main(){
  float numbers[3];
  printf("Introduce tres números y te diré si lo hiciste en orden decreciente\n");
  for(int i =0; i<3; i++){
    scanf("%f", &numbers[i]);
  }
   for(int i =0; i<3-1; i++){
     if(numbers[i] < numbers[i+1]){
       printf("No se han introducido en orden decreciente\n");
       break;
     }
     if(i==3-2){
       printf("Se han introducido en orden decreciente\n");
     }
  }
  return 0;
}
