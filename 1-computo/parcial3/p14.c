/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para introducir 10 números y saber cuál es el mayor y el menor
 */
#include <stdio.h>
int main(){
  float numbers[10];
  float mayor;
  float menor;
  printf("Introduce 10 números y te diré cuál es el mayor y el menor\n");
  for(int i =0; i<10; i++){
    scanf("%f", &numbers[i]);
  }
  mayor = numbers[0];
  menor = numbers[0];
   for(int i =1; i<10; i++){
     if(numbers[i] > mayor){
       mayor = numbers[i];
     }
     if(numbers[i] < menor){
       menor = numbers[i];
     }
  }
   printf ("mayor: %f\nmenor:%f\n", mayor, menor); 
  return 0;
}
