/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para saber si un número es mayor que 100
 */
#include <stdio.h>
int main(){
  float numero;
  printf("Introduce un número y te diré si es mayor que 100\n");
  scanf("%f",&numero);
  if(numero > 100) {
    printf("Es mayor que 100\n");
  }
  else {
    printf("No es mayor que 100\n");
  }
  return 0;
}
