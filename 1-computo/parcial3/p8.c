/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para saber si un número es positivo o negativo (o cero)
 */
#include <stdio.h>
int main(){
  float numero;
  printf("Introduce un número y te diré si es positivo, negativo o cero\n");
  scanf("%f",&numero);
  if(numero > 0) {
    printf("Positivo\n");
  }
  else if( numero < 0){
    printf("Negativo\n");
  }else {
    printf("Cero\n");
  }
  return 0;
}
