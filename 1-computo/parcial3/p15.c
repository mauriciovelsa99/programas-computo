/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para saber si el tercer número es igual a la suma de los otros dos
 */
#include <stdio.h>
int main(){
  float numbers[3];
  printf("Introduce tres números y te diré si el último es la suma de los otros dos\n");
  for(int i =0; i<3; i++){
    scanf("%f", &numbers[i]);
  }

  if(numbers[2] == numbers[1]+ numbers[0]){
     printf("Es la suma de los otros dos\n");
   }
   else{
     printf("No es la suma de los otros dos\n");
   }
  return 0;
}
