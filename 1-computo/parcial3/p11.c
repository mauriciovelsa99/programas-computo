/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa que detecta si introduces una vocal
 */
#include <stdio.h>
#include <string.h>
int main(){
  char c;
  char vowels[] = "aeiou";
  printf("Introduce un caracter\n");
  scanf(" %c", &c);
  if( strchr(vowels, c) != NULL){
    printf("Es una vocal\n");
  }else{
    printf("No es una vocal\n");
  }
  return 0;
}
