/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para saber el día de la semana dado un número
 */
#include <stdio.h>
int main(){
  int numero;
  do{
  printf("Escribe un número entero del 1 al 7 y te diré el día de la semana correspondiente (domingo == 1)\n");
  scanf("%d",&numero);
  }while(numero < 1 || numero > 7);
  switch(numero)
    {
    case 1:
      printf("Domingo\n");
      break;
    case 2:
      printf("Lunes\n");
      break;
    case 3:
      printf("Martes\n");
      break;
    case 4:
      printf("Miércoles\n");
      break;
    case 5:
      printf("Jueves\n");
      break;
    case 6:
      printf("Viernes\n");
      break;
    case 7:
      printf("Sábado\n");
      break;
    }  

  return 0;
}
