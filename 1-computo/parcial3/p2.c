/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para saber si un numero entero es par o impar
 */
#include <stdio.h>
int main(){
  int numero;
  printf("Escribe un número entero y te diré si es par o impar\n");
  scanf("%d",&numero);
  if(numero%2==0){
    printf("El número es par\n");
  }
  else{
    printf("El número es impar\n");
  }
  return 0;
}
