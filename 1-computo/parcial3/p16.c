/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para mostrar un menú
 */
#include <stdio.h>
int main(){
  int eleccion;
  do {
    printf("--------------MENU-----------------\n");
    printf("Archivo - 1\n");
    printf("Buscar - 2\n");
    printf("Salir - 0\n");
    scanf("%d", &eleccion);
    if (eleccion !=1 && eleccion !=2 && eleccion != 0) printf("Esa opción no existe, por favor ingresa algo válido!!!!!!\n");
  }while(eleccion != 0);
  return 0;
}
