/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa que sólo permite introducir los caracteres S y N
 */
#include <stdio.h>
int main(){
  char c;
  do {
  printf("Introduce S o N\n");
  scanf(" %c", &c);
    } while(c != 'N' && c!='S');
  printf("Gracias\n");
  return 0;
}
