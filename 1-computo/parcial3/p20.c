/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para saber si el tercer número es igual al resto de la división de los otros dos (se toman en cuenta los dos casos de división, a/b y b/a)
 */
#include <stdio.h>
int main(){
  int numbers[3];
  printf("Introduce tres números y te diré si el último es el resto de la división de los otros dos\n");
  for(int i =0; i<3; i++){
    scanf("%d", &numbers[i]);
  }

  if(numbers[2] == numbers[1] % numbers[0] || numbers[2] == numbers[0] % numbers[1] ){
     printf("Es el resto de la división de los otros dos\n");
   }
   else{
     printf("No es el resto de la división de los otros dos\n");
   }
  return 0;
}
