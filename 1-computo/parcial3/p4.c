/*
Fecha: 12 de mayo del 2020
Autor: Mauricio Vélez
Descripción: Programa para saber el mes dado un número
 */
#include <stdio.h>
int main(){
  int numero;
  do{
  printf("Escribe un número entero del 1 al 12 y te diré el mes correspondiente\n");
  scanf("%d",&numero);
  }while(numero < 1 || numero > 12);
  switch(numero)
    {
    case 1:
      printf("Enero\n");
      break;
    case 2:
      printf("Febrero\n");
      break;
    case 3:
      printf("Marzo\n");
      break;
    case 4:
      printf("Abril\n");
      break;
    case 5:
      printf("Mayo\n");
      break;
    case 6:
      printf("Junio\n");
      break;
    case 7:
      printf("Julio\n");
      break;
    case 8:
      printf("Agosto\n");
      break;
    case 9:
      printf("Septiembre\n");
      break;
    case 10:
      printf("Octubre\n");
      break;
    case 11:
      printf("Noviembre\n");
      break;
    case 12:
      printf("Diciembre\n");
      break;

    }  

  return 0;
}
