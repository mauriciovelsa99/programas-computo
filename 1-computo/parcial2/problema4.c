/*
Autor: Mauricio Vélez
Fecha: "2/Abr/2020
Descripción: Intercambia valores de 3 variables
*/
#include <stdio.h>

int main(){
  int a,b,c;
  printf("Ingrese los valores de las tres variables en una sola linea, separadas por espacios\n");
  scanf ("%d %d %d", &a ,&b, &c);
  printf("Valores iniciales:\n");
  printf("a: %d\n", a);
  printf("b: %d\n", b);
  printf("c: %d\n", c);
  //intercambio el valor de a y b
  a = a+b;
  b = a-b;
  a = a-b;
  //intercambio el valor de a y c (que es lo mismo que hacerlo con b y c iniciales)
  a = a+c;
  c = a-c;
  a = a-c;
  printf("Valores finales (se recorrió un lugar cada valor):\n");
  printf("a: %d\n", a);
  printf("b: %d\n", b);
  printf("c: %d\n", c);
  return 0;
}
