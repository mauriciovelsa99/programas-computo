/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: Dados el radio y la altura, calcula el área de un cilindro
*/
#include <stdio.h>
#define PI 3.14159265358979323846
int main(){
  float radio, altura, areabase, area;
  printf("Ingrese el radio de la base\n");
  scanf ("%f", &radio);
  printf("Ingrese la altura del cilindro\n");
  scanf ("%f", &altura);
  areabase = radio* radio* PI;
  area =  areabase*altura;
  printf("El área del cilindro es de: %.2f unidades cúbicas\n", area);
  return 0;
}
