/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: operaciones aritméticas con dos enteros a y b
*/
#include <stdio.h>

int main(){
    int a,b;
    printf("Ingrese los dos enteros a y b \n");
    scanf("%d %d", &a, &b);
    
    printf("suma: %d\n", a+b);
    printf("resta de a menos b: %d\n", a-b);
    printf("multiplicación: %d\n", a*b);
    printf("division del primero entre el segundo: %f\n", (float)a/b);
    return 0;
}
