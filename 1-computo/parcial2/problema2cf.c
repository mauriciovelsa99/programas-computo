#include <stdio.h>
/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: De celsius a farenheit
*/
int main(){
  float celsius, farenheit;
    printf("Ingrese los grados celsius \n");
    scanf("%f", &celsius);
    farenheit = (celsius*9/5)+32;
    printf("grados Farenheit: %f \n", farenheit);
    return 0;
}
