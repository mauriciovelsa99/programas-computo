/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: Dada la ecuación de los términos de una progresión aritmética: u = a + (n-1)r, este programa calcula r dados u, a y n
*/
#include <stdio.h>
int main(){
  float u,a,n,r;
  printf("El n-esimo término de una sucesion aritmetica esta dado por u = a+(n-1)r\n");
  printf("Ingrese u,a,n separados por espacios\n");
  scanf ("%f %f %f", &u, &a, &n);
  /*Para obtener r en términos de las 3 variables, basta con despejar la ecuación:
u-a = (n-1)r
(u-a)/ (n-1) = r
*/
     r = (u-a)/(n-1);
  printf("El valor de r es: %.2f\n", r);
  return 0;
}
