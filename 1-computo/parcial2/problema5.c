/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: Dadas dos coordenadas, calcula la ecuación de la recta que pasa por ellos
*/
#include <stdio.h>

int main(){
  float x1,x2,y1,y2,m;
  printf("Ingrese la primer coordenada con los valores de x e y separados por espacios\n");
  scanf ("%f %f", &x1 ,&y1);
  printf("Ingrese la segunda coordenada con los valores de x e y separados por espacios\n");
  scanf ("%f %f", &x2 ,&y2);
  m = (x2-x1)/(y2-y1);
  
  printf("La ecuación que pasa por esos dos puntos tiene la forma:\n");
  printf("y = %.2fx+ %.2f", m,(m*-x1)+y1 );
  return 0;
}
