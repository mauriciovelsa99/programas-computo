#include <stdio.h>
/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: De farenheit a celsius
*/
int main(){
  float celsius, farenheit;
    printf("Ingrese los grados farenheit \n");
    scanf("%f", &farenheit);
    celsius = (farenheit - 32)*5/9;
    printf("grados Celsius: %f \n", celsius);
    return 0;
}
