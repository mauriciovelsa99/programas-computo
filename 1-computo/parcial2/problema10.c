/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción:  Una empresa produce fertilizantes. Cada mes el ingreso por ventas en miles de
dolares se describe con v = 0.4x(30− x) mientras que el costo de producción en miles de dólares es v = 5+10ln(x),
siendo la cantidad producida en toneladas, 1 < x < 30.

Este programa calcula el ingreso neto para un valor de x
*/
#include <stdio.h>
#include <math.h>
int main(){
  float ingreso_neto, ingreso, costo, x;
  do {
    printf("Ingresa un valor de x, con la condicion 1<x<30\n");
    scanf("%f", &x);
  }while( x<=1|| x>=30);
  ingreso = 0.4*x*(30-x);
  costo = 5+10*log(x);
  ingreso_neto = ingreso -costo;
  printf("El ingreso neto es de: %f\n", ingreso_neto);
  return 0;
}
