/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: Dada la ecuación de los términos de una progresión aritmética: u = a + (n-1)r, este programa calcula n dados u,a y r
*/
#include <stdio.h>
int main(){
  float u,a,n,r;
  printf("El n-esimo término de una sucesion aritmetica esta dado por u = a+(n-1)r\n");
  printf("Ingrese u,a,r separados por espacios\n");
  scanf ("%f %f %f", &u, &a, &r);
  /*Para obtener n en términos de las 3 variables, basta con despejar la ecuación:
u-a = (n-1)r
(u-a)/ r = n-1
(u-a)/r+1 = n
*/
     n = (u-a)/r+1;
  printf("El valor de n es: %.2f\n", n);
  return 0;
}
