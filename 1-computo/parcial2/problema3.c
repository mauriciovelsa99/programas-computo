/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: Programa que obtiene el area de circunferencia dado su radio
 */
#include <stdio.h>
#define PI 3.14159265358979323846
int main(){
  float radio, area;
  printf("Ingrese el radio de la circunferencia\n");
  scanf("%f", &radio);
  area = PI*radio*radio;
  printf("El área de la circunferencia de radio %f es: %f\n", radio, area);
  return 0;
}
