/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: Un banco acumula capital mensulmente con el siguiente modelo A = P [ [ (1+x)^n -1] / [x] ] Donde:
A = valor acumulado
P = valor de cada depósito mensual
n = cantidad de depositos mensuales
x = Tasa de interes mensual

Este programa calcula el valor acuumulado A, usando P,n,x
*/
#include <stdio.h>
#include <math.h>
int main(){
  float a,p,n,x;
  printf("Para saber el valor acumulado de un capital, debes ingresar los siguientes datos.\n");
  printf("Ingresa P(depósito mensual), n(cantidad de depositos mensuales), x(tasa de interes mensual) separados por espacios\n");
  scanf("%f %f %f", &p, &n, &x);
  a = p * ( ( pow(1+x,n) -1) / x );
    printf("El valor acumulado será de: %f\n", a);
  return 0;
}
