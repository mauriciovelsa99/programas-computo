/*
Autor: Mauricio Vélez
Fecha: 2/Abr/2020
Descripción: Un modelo de crecimiento poblacional esta dado por n = 5t + e^(0.1t) donde n es el numero de habitantes y t el tiempo en años, calcular el numero de habitantes en t años
*/
#include <stdio.h>
#include <math.h>
int main(){
  float t, n;
  printf("Un modelo de crecimiento poblacional esta dado por n = 5t + e^(0.1t) donde n es el numero de habitantes y t el tiempo en años\n");
  printf("Ingresa t\n");
  scanf("%f", &t);
  n = 5*t + exp(0.1*t);
  printf("La poblacion en %.1f años será de %.0f habitantes\n", t,n);
  return 0;
}
